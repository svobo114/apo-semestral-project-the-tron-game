#include "classes.hpp"
#include "maps.hpp"
#include "mem.hpp"
#include "drawing.hpp"
#include "main_functions.hpp"

#include"gl.hpp"

using namespace std;

#define ERR_scaling_not_correct -2
#define ERR_memory_init_failed -1


int main()
{
    //fb init
    fb = (unsigned short *)calloc(480 * 320, sizeof(unsigned short)); 

    //mem init
    mem_base = (unsigned char *)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    parlcd_mem_base = (unsigned char *)map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    mem_base_knobs = (unsigned char *)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base_knobs == NULL || parlcd_mem_base == NULL || mem_base == NULL)
    {
        cerr << "err: mapping phys address failed" << endl;
        exit(ERR_memory_init_failed);
    }

    // lcd init
    parlcd_hx8357_init(parlcd_mem_base);


    while (true)  
    {
        clean_buffer();

        draw_rectangle(65, 100, 125, 35, colour_head);

        draw_rectangle(265, 140, 40, 30, colour_blue);

        draw_rectangle(265, 180, 40, 30, colour_blue);

        draw_rectangle(265, 220, 40, 30, colour_blue);

        print_buffer();

        draw_menu();    

        switch (counter_scale)
        {
        case 0:
            scale = 5;
            break;
        case 1:
            scale = 10;
            break;
        case 2:
            scale = 20;
            break;
        }
        
        //init board
        Board board(scale);
        if (board.initialized == false){return ERR_scaling_not_correct;}

        switch (counter_speed)
        {
        case 0:
            speed = 1000;
            break;
        case 1:
            speed = 100;
            break;
        case 2:
            speed = 5000;
            break;
        }

        if (end_game != 0)
        {
            for (int i = 0; i < total_index; i++)
            {
                fb[i] = 0;
            }
            print_buffer();
            break;
        }

        switch (counter_map)
        {
        case 0:
            printf("You're here\n");
            break;
        case 1:
            printf("You're 1 %d\n", scale);
            initiate_map1(scale, board.main_positions, board.colour_buffer);
            break;
        case 2:
            printf("You're 2\n");
            initiate_map2(scale, board.main_positions, board.colour_buffer);
            break;
        }

        //player init
        Player p1 = Player(1);
        Player p2 = Player(2);
        p1.player_init(board.board_size_x / 6, board.board_size_y / 2, colour1, 'r'); 
        p2.player_init(board.board_size_x / 6 * 5, board.board_size_y / 2, colour2, 'l');

        //davam hrace do position array
        board.main_positions.at(p1.y).at(p1.x) = 1;
        board.main_positions.at(p2.y).at(p2.x) = 2;
        // Davam hrace do array s color
        board.colour_buffer.at(p1.y).at(p1.x) = p1.colour;
        board.colour_buffer.at(p2.y).at(p2.x) = p2.colour;
        // prekreslim do frame bufferu
        board.draw_to_buffer();

        //main game loop
        game(board, p1, p2);

        //after the main game
        clean_buffer();
        draw_string("Game Over", 120, 100, 3);
        print_buffer();

        counter_map = 0;
        counter_scale = 0;
        counter_speed = 0;
        counter = 0;

        sleep(2);
    }

    free(fb);
}
