
#include "classes.hpp"
#include "mem.hpp"
#include "main_functions.hpp"

#ifndef _MEM_INC
    #define _MEM_INC

    #include "mzapo_parlcd.h"
    #include "mzapo_phys.h"
    #include "mzapo_regs.h"
    #include "font_types.h"

#endif 

    extern int speed;
    extern int uint_val;
    extern int int_val;

    extern uint32_t r;
    extern unsigned char *mem_base;
    extern unsigned char *mem_base_knobs;


    extern unsigned short colour_head;

    extern unsigned int red_prev, red_now ;
    extern unsigned int blue_prev, blue_now ;
    extern int blue_diff, red_diff , green_diff;


void game(Board board, Player p1, Player p2)
{

    timespec game_delay = {.tv_sec = 0, .tv_nsec = 500 * 1000 * speed};
    

    *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB1_o) = 0xff0000;

    *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB2_o) = 0xff0000;

    r = *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_KNOBS_8BIT_o);

    uint_val = r;
    red_prev = ((uint_val >> 16) & 0xff);
    blue_prev = ((uint_val)&0xff);

    while (true) // gameplay loop
    {
        r = *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_KNOBS_8BIT_o);


        *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_LINE_o) = r;

        *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB1_o) = 0xff0000;

        *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB2_o) = 0x0000f3;

        int_val = r;
        uint_val = r;

        // Dostaneme cislo kazdeho tlacitka
        red_now = ((uint_val >> 16) & 0xff);
        blue_now = ((uint_val)&0xff);

        //vypocet pohybu
        red_diff = red_now - red_prev;
        blue_diff = blue_now - blue_prev;
        
        //zaznaceni playeru do boardu=necham za sebou ocasek
        board.colour_buffer.at(p1.y).at(p1.x) = p1.colour;
        board.colour_buffer.at(p2.y).at(p2.x) = p2.colour;

        if (red_diff > 1 && red_diff < 5 || (red_prev == 252 && red_diff < -1 && (red_diff > -5)))
        {

            p1.change_direction('r');
            printf("red > 3\n");
        }
        else if ((red_diff < -1) && (red_diff > -5) || ((red_prev < 4 || red_prev > 252) && red_diff > 1) && red_diff < 5)
        {
            p1.change_direction('l');
        }
        else
        {
            p1.change_direction('n');
        }

        if (blue_diff > 1 && blue_diff < 5 || (blue_prev == 252 && blue_diff < -1 && (blue_diff > -5)))
        {
            p2.change_direction('r');
            // jedna strana
        }
        else if ((blue_diff < -1) && (blue_diff > -5) || ((blue_prev < 4 || blue_prev > 252) && blue_diff > 1) && blue_diff < 5)
        {
            p2.change_direction('l');
            // drauha strana
        }
        else
        {
            p2.change_direction('n');
        }

        // Check jestli uz je konec framu
        if (p1.x < 0)
        {
            p1.x = board.board_size_x - 1;
        }
        if (p1.x >  board.board_size_x - 1)
        {
            p1.x = 0;
        }
        if (p1.y < 0)
        {
            p1.y =  board.board_size_y - 1;
        }
        if (p1.y > board.board_size_y - 1)
        {
            p1.y = 0;
        }
        if (p2.x < 0)
        {
            p2.x =  board.board_size_x - 1;
        }
        if (p2.x >  board.board_size_x - 1)
        {
            p2.x = 0;
        }
        if (p2.y < 0)
        {
            p2.y = board.board_size_y - 1;
        }
        if (p2.y > board.board_size_y - 1)
        {
            p2.y = 0;
        }

        printf("x = %d\n", board.board_size_x - 1);
        if (board.main_positions.at(p1.y).at(p1.x) != 0)
        {
            *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB1_o) = 0x0000f3;
            winning_lights();
            break;
        }
        if (board.main_positions.at(p2.y).at(p2.x) != 0)
        {
            *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB2_o) = 0xff0000;
            winning_lights();
            break;
        }

        //davam hrace do colour array
        board.main_positions.at(p1.y).at(p1.x) = p1.signature;
        board.main_positions.at(p2.y).at(p2.x) = p2.signature;

        // Davam hlavu hrace do array s color
        board.colour_buffer.at(p1.y).at(p1.x) = colour_head;
        board.colour_buffer.at(p2.y).at(p2.x) = colour_head;

        red_prev = red_now;
        blue_prev = blue_now;

        board.draw_to_buffer();                  // prekreslim do frame bufferu
        print_buffer();

        clock_nanosleep(CLOCK_MONOTONIC, 0, &game_delay, NULL);
    }
}
