using namespace std;

//std C knihovny
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

//std C++ knihovny
#include <iostream>
#include <vector>

#ifndef _MEM_INC
    #define _MEM_INC

    #include "mzapo_parlcd.h"
    #include "mzapo_phys.h"
    #include "mzapo_regs.h"
    #include "font_types.h"

#endif 


#ifndef _GLOBAL_VAR
    #define _GLOBAL_VAR

    int height=320;
    int width=480;
    int total_index=320*480;

    //env. variables
    int scale = 5;
    unsigned short *fb;

    //rychlost hry
    int speed = 250; 

    //na výpočet pohybu
    unsigned int red_prev = 0, red_now = 0;
    unsigned int green_prev = 0, green_now = 0;
    unsigned int blue_prev = 0, blue_now = 0;
    int blue_diff = 0, red_diff = 0, green_diff = 0;

    //memory alokace
    uint32_t r;
    unsigned char *mem_base;
    unsigned char *parlcd_mem_base;
    unsigned char *mem_base_knobs;

    int int_val;
    unsigned int uint_val;

    //menu countery a options
    int stop = 1, stop_blue = 1;
    int end_menu = 0, end_game = 0;
    int counter_scale = 0, counter_speed = 0, counter_map = 0;
    int counter = 0;    

    //barvy hracu
    unsigned short colour1 = 0xff000;
    unsigned short colour2 = 0x7ff;
    unsigned short colour_head = 0x7f0;
    unsigned short colour_blue = 0x7ff;

#endif 
