
#include "drawing.hpp"
#include <time.h>
#include "mem.hpp"
#include "main_functions.hpp"

#ifndef _MEM_INC
    #define _MEM_INC

    #include "mzapo_parlcd.h"
    #include "mzapo_phys.h"
    #include "mzapo_regs.h"
    #include "font_types.h"

#endif 

extern int speed;
    extern int uint_val;
    extern int int_val;

    extern uint32_t r;
    extern unsigned char *mem_base;
    extern unsigned char *mem_base_knobs;


    extern unsigned short colour_head;

    extern unsigned int green_prev , green_now ;
    extern unsigned int red_prev, red_now ;
    extern unsigned int blue_prev, blue_now ;
    extern int blue_diff, red_diff , green_diff;

    extern int stop , stop_blue ;
    extern int end_menu , end_game ;
    extern int counter_scale , counter_speed , counter_map ;
    extern int counter ;    


//update counteru omezeneho velikosti, napr. na vyber v menu
int update_counter(int coun, int max)
{

    if (coun < 0)
    {
        coun = max;
    }
    if (coun > max)
    {
        coun = 0;
    }
    return coun;
}

void draw_menu()
{

    struct timespec menu_delay = {.tv_sec = 0, .tv_nsec = 500 * 1000 * 1000}; 
    
    *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB1_o) = 0xff0000;
    *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_LED_RGB2_o) = 0xff0000;

    r = *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_KNOBS_8BIT_o);

    uint_val = r;
    green_prev = ((uint_val >> 8) & 0xff);
    blue_prev = ((uint_val)&0xff);

    while (true)
    {
        r = *(volatile uint32_t *)(mem_base_knobs + SPILED_REG_KNOBS_8BIT_o);
        uint_val = r;

        if ((uint_val & 0x7000000) != 0)
        {
            switch (counter)
            {
            case 0:
                end_menu = 1;
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                end_game = 1;
                break;
            }
        }

        if (end_menu != 0 || end_game != 0)
        {
            end_menu = 0;
            break;
        }

        blue_now = ((uint_val)&0xff);
        blue_diff = blue_now - blue_prev;
        green_now = ((uint_val >> 8) & 0xff);
        green_diff = green_now - green_prev;

        printf("counter = %d scale = %d  speed = %d map = %d\n", counter, counter_scale, counter_speed, counter_map);
        if (green_diff > 1 || (green_prev > 252 && green_diff < -1))
        {
            counter++;
            stop = 0;
        }
        else if ((green_diff < -1) || ((green_prev < 4 || green_prev > 252) && green_diff > 1))
        {
            counter--;
            stop = 0;
        }
        else
        {
            stop = 1;
        }

        if (blue_diff > 1 || (blue_prev > 252 && blue_diff < -1))
        {
            switch (counter)
            {
            case 1:
                counter_scale++;
                printf("scale++\n");
                break;
            case 2:
                counter_speed++;
                printf("speed++\n");
                break;
            case 3:
                counter_map++;
                printf("map++\n");
                break;
            }
            stop_blue = 0;
        }
        else if ((blue_diff < -1) || ((blue_prev < 4 || blue_prev > 252) && blue_diff > 1))
        {
            switch (counter)
            {
            case 1:
                counter_scale--;
                printf("scale--\n");
                break;
            case 2:
                counter_speed--;
                printf("speed--\n");
                break;
            case 3:
                counter_map--;
                printf("map--\n");
                break;
            }
            stop_blue = 0;
        }
        else
        {
            stop_blue = 1;
        }

        counter_scale = update_counter(counter_scale, 2);
        counter_speed = update_counter(counter_speed, 2);
        counter_map = update_counter(counter_map, 2);

        // Hlavni moznosti
        draw_string("Tron", 160, 10, 5);
        draw_string("Play", 70, 100, 2);
        draw_string("Scale", 70, 140, 2);
        draw_string("Speed", 70, 180, 2);
        draw_string("Map", 70, 220, 2);
        draw_string("Quit", 70, 260, 2);

        // Scale options
        draw_string("5", 270, 140, 2);
        draw_string("10", 320, 140, 2);
        draw_string("20", 370, 140, 2);

        // Speed options
        draw_string("1x", 270, 180, 2);
        draw_string("2x", 320, 180, 2);
        draw_string("3x", 370, 180, 2);

        // Map options
        draw_string("1", 270, 220, 2);
        draw_string("2", 320, 220, 2);
        draw_string("3", 370, 220, 2);

        if (stop == 0)
        {
            counter = update_counter(counter, 4);
            draw_green_rectangle();
        }
        switch (counter)
        {
        case 1:
            draw_blue_rectangle(counter_scale, 1);
            break;
        case 2:
            draw_blue_rectangle(counter_speed, 2);
            break;
        case 3:
            draw_blue_rectangle(counter_map, 3);
            break;
        }
        green_prev = green_now;
        blue_prev = blue_now;
        print_buffer();

        // Check if we change polohu tlacitka
        stop = 1;
        stop_blue = 1;

        clock_nanosleep(CLOCK_MONOTONIC, 0, &menu_delay, NULL);
    }
}
