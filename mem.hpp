



#ifndef _MEM_FUNC
    #define _MEM_FUNC

    //fce, ktere si hraji s pameti, nebo s bufferem a nemaji kam jinam jit
    using namespace std;

    //printne fb
    void print_buffer();

    //vycisti fb
    void clean_buffer();

    //rozsviti led pas, kdyz skonci hra
    void winning_lights(); // jedu nahoru a dolu po array

#endif 