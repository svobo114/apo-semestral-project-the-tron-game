
#include "classes.hpp"

using namespace std;

#ifndef _MAIN_FUNK
    #define _MAIN_FUNK
    //small func. for updating counter used in big functions
    int update_counter(int coun, int max);

    //velkafunkce, ktera uvnitr dela hlavni hru
    void game(Board board, Player p1, Player p2);

    //velka funkce, ktera zodpovida za beh hlavniho menu
    void draw_menu();

#endif