
#include "classes.hpp"

using namespace std;

    Player::Player(int player_id)
    {
        signature = player_id;
    }

    void Player::player_init(int initial_x, int initial_y, unsigned short new_colour, char starting_direction)
    {
        x = initial_x;
        y = initial_y;
        colour = new_colour;
        direction = starting_direction;
    }

    void Player:: change_direction(char l_or_r)
    {
        switch (l_or_r)
        {
        case 'l':
            switch (direction)
            {
            case 'l':
                direction = 'd';
                y++;
                break;
            case 'r':
                direction = 'u';
                y--;
                break;
            case 'u':
                direction = 'l';
                x--;
                break;
            case 'd':
                direction = 'r';
                x++;
                break;
            }
            break;
        case 'r':
            switch (direction)
            {
            case 'l':
                direction = 'u';
                y--;
                break;
            case 'r':
                direction = 'd';
                y++;
                break;
            case 'u':
                direction = 'r';
                x++;
                break;
            case 'd':
                direction = 'l';
                x--;
                break;
            }
            break;
        case 'n':
            switch (direction)
            {
            case 'l':
                x--;
                break;
            case 'r':
                x++;
                break;
            case 'u':
                y--;
                break;
            case 'd':
                y++;
                break;
            }
            break;
        default:
            cerr << "Given Invalid directional input!\n";
            break;
        }
    }
