
#include "classes.hpp"
    extern int scale;
    extern unsigned short *fb;
    extern int total_index;

using namespace std;

    Board::Board(int scaling)
    {
        if (320 % scaling != 0 || 480 % scaling != 0)
        {
            cerr << "Error: lcd size not divisible by scaling factor!\n";
            return;
        }

        matrix_scaling = scaling;

        board_size_x = 480 / scaling;
        board_size_y = 320 / scaling;

        main_positions = vector<vector<char>>(board_size_y, vector<char>(board_size_x, 0));
        colour_buffer = vector<vector<unsigned short>>(board_size_y, vector<unsigned short>(board_size_x, 0));
        initialized = true;
    }

    void Board:: draw_to_buffer()
    {
        // note: muze byt efektivnejsi vzhledem k cachovani
        for (int i = 0; i < total_index; i++)
        {
            int y = (i / 480) / scale;
            int x = (i % 480) / scale;
            fb[i] = colour_buffer.at(y).at(x);
        }
    }



