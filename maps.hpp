#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

#ifndef _MAPS
    #define _MAPS
    void initiate_map1(int scale,
                    vector<vector<char>> & map_borders,
                    vector<vector<unsigned short>> & col_palette);


    void initiate_map2(int scale,
                    vector<vector<char>> & map_borders,
                    vector<vector<unsigned short>> & col_palette);
#endif 