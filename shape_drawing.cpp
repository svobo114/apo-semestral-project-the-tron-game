
    extern int scale;
    extern unsigned short *fb;
    extern unsigned short colour_head;
    extern unsigned short colour_blue ;
    extern int counter ; 

#include "drawing.hpp"

using namespace std;

void draw_rectangle(int x, int y, int length, int hight, unsigned short col)
{
    for (int i = x + y * 480; i < x + y * 480 + length; i++)
    {
        fb[i] = col;
    }
    for (int i = x + y * 480 + 480 * hight; i < x + y * 480 + 480 * hight + length; i++)
    {
        fb[i] = col;
    }
    for (int i = 0; i < hight; i++)
    {
        fb[x + y * 480 + i * 480] = col;
    }
    for (int i = 0; i < hight; i++)
    {
        fb[x + length + y * 480 + i * 480] = col;
    }
}

void clean_rectangle(int x, int y, int length, int hight)
{
    for (int i = x + y * 480; i < x + y * 480 + length; i++)
    {
        fb[i] = 0;
    }
    for (int i = x + y * 480 + 480 * hight; i < x + y * 480 + 480 * hight + length; i++)
    {
        fb[i] = 0;
    }
    for (int i = 0; i < hight; i++)
    {
        fb[x + y * 480 + i * 480] = 0;
    }
    for (int i = 0; i < hight; i++)
    {
        fb[x + length + y * 480 + i * 480] = 0;
    }
}

void draw_green_rectangle()
{
    switch (counter)
    {
    case 0:
        clean_rectangle(65, 255, 125, 35);
        clean_rectangle(65, 140, 125, 35);
        draw_rectangle(65, 100, 125, 35, colour_head);
        break;
    case 1:
        clean_rectangle(65, 100, 125, 35);
        clean_rectangle(65, 180, 125, 35);
        draw_rectangle(65, 140, 125, 35, colour_head);
        break;
    case 2:
        clean_rectangle(65, 140, 125, 35);
        clean_rectangle(65, 220, 125, 35);
        draw_rectangle(65, 180, 125, 35, colour_head);
        break;
    case 3:
        clean_rectangle(65, 180, 125, 35);
        clean_rectangle(65, 255, 125, 35);
        draw_rectangle(65, 220, 125, 35, colour_head);
        break;
    case 4:
        clean_rectangle(65, 220, 125, 35);
        clean_rectangle(65, 100, 125, 35);
        draw_rectangle(65, 255, 125, 35, colour_head);
        break;
    }
}

void draw_blue_rectangle(int coun, int line)
{
    printf("coun = %d  line = %d\n", coun, line);
    switch (line)
    {
    case 1:
        switch (coun)
        {
        case 0:
            clean_rectangle(365, 140, 40, 30);
            clean_rectangle(315, 140, 40, 30);
            draw_rectangle(265, 140, 40, 30, colour_blue);
            break;
        case 1:
            clean_rectangle(265, 140, 40, 30);
            clean_rectangle(365, 140, 40, 30);
            draw_rectangle(315, 140, 40, 30, colour_blue);
            break;
        case 2:
            clean_rectangle(315, 140, 40, 30);
            clean_rectangle(265, 140, 40, 30);
            draw_rectangle(365, 140, 40, 30, colour_blue);
            break;
        }
        break;
    case 2:
        switch (coun)
        {
        case 0:
            clean_rectangle(365, 180, 40, 30);
            clean_rectangle(315, 180, 40, 30);
            draw_rectangle(265, 180, 40, 30, colour_blue);
            break;
        case 1:
            clean_rectangle(265, 180, 40, 30);
            clean_rectangle(365, 180, 40, 30);
            draw_rectangle(315, 180, 40, 30, colour_blue);
            break;
        case 2:
            clean_rectangle(315, 180, 40, 30);
            clean_rectangle(265, 180, 40, 30);
            draw_rectangle(365, 180, 40, 30, colour_blue);
            break;
        }
        break;
    case 3:
        switch (coun)
        {
        case 0:
            clean_rectangle(365, 220, 40, 30);
            clean_rectangle(315, 220, 40, 30);
            draw_rectangle(265, 220, 40, 30, colour_blue);
            break;
        case 1:
            clean_rectangle(265, 220, 40, 30);
            clean_rectangle(365, 220, 40, 30);
            draw_rectangle(315, 220, 40, 30, colour_blue);
            break;
        case 2:
            clean_rectangle(315, 220, 40, 30);
            clean_rectangle(265, 220, 40, 30);
            draw_rectangle(365, 220, 40, 30, colour_blue);
            break;
        }
        break;
    }
}
