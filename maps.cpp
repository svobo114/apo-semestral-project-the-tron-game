
#include "maps.hpp"

using namespace std;

unsigned short wall_colour=-1;

//pro debugging
void print_map(vector<vector<char>> border_map,int x_scaled,int y_scaled){

    for(int i=0;i<x_scaled;i++){
    }
    cerr<<"\n";

    for (int i = 0; i < y_scaled; i++)
    {
        for (int j = 0; j < x_scaled; j++)
        {
            char ch=border_map.at(i).at(j);
            if(ch==0){
                cerr<<"_";
            }
            cerr<<ch;
        }
        cerr<<"\n";
    }
    for(int i=0;i<x_scaled;i++){
    }
    cerr<<"\n";

    cerr<<endl;//flushne výpis a udělá \n

}

void initiate_map1(int scale,
                   vector<vector<char>> & map_borders,
                   vector<vector<unsigned short>> & col_palette)
{
    int x_scaled = 480 / scale;
    int y_scaled = 320 / scale;
    int scaled_index = x_scaled * y_scaled;

    for (int i = 0; i < y_scaled; i++)
    {
        for (int j = 0; j < x_scaled; j++)
        {
            if(i==0 ||i==1 || j==0|| j==1 || i==y_scaled-1 || i==y_scaled-2 ||  j == x_scaled-1 || j == x_scaled-2){
                map_borders.at(i).at(j) = 100;
                col_palette.at(i).at(j) = wall_colour;
            }else{
                map_borders.at(i).at(j)= 0;
                col_palette.at(i).at(j)=0;//barva pozadi
            }
        }
    }
}


void initiate_map2(int scale,
                   vector<vector<char>> & map_borders,
                   vector<vector<unsigned short>> & col_palette)
{

    int x_scaled = 480 / scale;
    int y_scaled = 320 / scale;
    int scaled_index = x_scaled * y_scaled;

    //mapa 2 má čáry uprostřed
    for (int i = 0; i < y_scaled; i++)
    {
        for (int j = 0; j < x_scaled; j++)
        {
            if ( i > y_scaled/3 && i <y_scaled/3*2 && (j==x_scaled/3 || j==x_scaled/3*2 || j==x_scaled/3-1 || j==x_scaled/3*2-1)){
                map_borders.at(i).at(j)= 100;
                col_palette.at(i).at(j)=wall_colour;
            }else{
                map_borders.at(i).at(j)= 0;
                col_palette.at(i).at(j)=0;//barva pozadi
            }
        }
    }
}

/*
int main(){ //ukazka uziti
    int scale =20;
    int x_scaled = 480 / scale;
    int y_scaled = 320 / scale;
    int scaled_index = x_scaled * y_scaled;

    vector<vector<char>> main_positions;          // resi kolize
    vector<vector<unsigned short>> colour_buffer; // resi barvy na vytisk. muze obsahovat pozadi

    main_positions = vector<vector<char>>(y_scaled, vector<char>(x_scaled, 0));
    colour_buffer = vector<vector<unsigned short>>(y_scaled, vector<unsigned short>(x_scaled, 0));

    //vybrána mapa 2
    initiate_map2(scale,main_positions,colour_buffer);

    print_map(main_positions,x_scaled,y_scaled);

    initiate_map1(scale,main_positions,colour_buffer);

    print_map(main_positions,x_scaled,y_scaled);
    
}*/