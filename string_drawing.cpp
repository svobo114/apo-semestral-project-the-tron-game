
#ifndef _MEM_INC
    #define _MEM_INC

    #include "mzapo_parlcd.h"
    #include "mzapo_phys.h"
    #include "mzapo_regs.h"
    #include "font_types.h"

#endif 

#include "drawing.hpp"

    extern int scale;
    extern unsigned short *fb;
    //font
    font_descriptor_t *fdes = &font_winFreeSystem14x16;
    unsigned short char_colour=0xff000;

using namespace std;

void draw_pixel(int x, int y, unsigned short color)
{
    if (x >= 0 && x < 480 && y >= 0 && y < 320)
    {
        fb[x + 480 * y] = color;
    }
}

int char_width(int ch)
{
    int w;
    if (!fdes->w)
    {
        w = fdes->maxwidth;
    }
    else
    {
        w = fdes->w[ch - fdes->firstchar];
    }
    return w;
}

void draw_pixel_big(int x, int y, unsigned short color, int scale_text)
{
    int i, j;
    for (i = 0; i < scale_text; i++)
    {
        for (j = 0; j < scale_text; j++)
        {
            draw_pixel(x + i, y + j, color);
        }
    }
}

void draw_char(int x, int y, char ch, unsigned short color, int scale_text)
{
    int w = char_width(ch);
    const font_bits_t *ptr;
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size))
    {
        if (fdes->offset)
        {
            ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
        }
        else
        {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->h];
        }
        int i, j;
        for (i = 0; i < fdes->h; i++)
        {
            font_bits_t val = *ptr;
            for (j = 0; j < w; j++) // jdu do sirky charakteru
            {
                if ((val & 0x8000) != 0)
                {
                    draw_pixel_big(x + scale_text * j, y + scale_text * i, color, scale_text);
                }
                val <<= 1;
            }
            ptr++;
        }
    }
}

void draw_string(string s, int x, const int y, int scale_text)
{

    for (int i = 0; i < s.length(); i++)
    {
        draw_char(x, y, s.at(i), char_colour, scale_text);          
        x += char_width(s.at(i)) * scale_text + 1 * scale_text; 

    }
}
