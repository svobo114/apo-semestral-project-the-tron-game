#include <iostream>
#include <vector>

using namespace std;

#ifndef _CLASSES
    #define _CLASSES

    class Player
    {
    public:
        unsigned short colour;
        int x, y;
        char direction; // bude zadane cislem
        char signature; // player id
        bool isDead = false;

        Player(int player_id);

        void player_init(int initial_x, int initial_y, unsigned short new_colour, char starting_direction);

        void change_direction(char l_or_r);

    };

    class Board
    {
    public:
        vector<vector<char>> main_positions;          // resi kolize
        vector<vector<unsigned short>> colour_buffer; // resi barvy na vytisk. muze obsahovat pozadi
        int board_size_x;
        int board_size_y;
        bool initialized = false;
        int matrix_scaling;

        Board(int scaling);

        void draw_to_buffer();

    };

#endif 
