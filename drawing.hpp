using namespace std;
#include <string>

#ifndef _DRAW
    #define _DRAW

    //draw znaky
    void draw_pixel(int x, int y, unsigned short color);
    int char_width(int ch);
    void draw_pixel_big(int x, int y, unsigned short color, int scale_text);
    void draw_char(int x, int y, char ch, unsigned short color, int scale_text);
    void draw_string(string s, int x, const int y, int scale_text);

    //draw tvary
    void draw_rectangle(int x, int y, int length, int hight, unsigned short col);
    void clean_rectangle(int x, int y, int length, int hight);
    void draw_green_rectangle();
    void draw_blue_rectangle(int coun, int line);

#endif 