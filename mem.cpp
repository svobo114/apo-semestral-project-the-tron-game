
#include <time.h>
#include "mem.hpp"
    extern int scale;
    extern unsigned short *fb;
    extern unsigned char *mem_base;
    extern unsigned char *parlcd_mem_base;
    extern int total_index;

#ifndef _MEM_INC
    #define _MEM_INC

    #include "mzapo_parlcd.h"
    #include "mzapo_phys.h"
    #include "mzapo_regs.h"
    #include "font_types.h"

#endif 

using namespace std;

//printne fb
void print_buffer(){
        parlcd_write_cmd(parlcd_mem_base, 0x2c); // write comand
        for (int i = 0; i < total_index; i++)
        {
            parlcd_write_data(parlcd_mem_base, fb[i]); // write data
        }
}

//vycisti fb
void clean_buffer()
{
    for (int i = 0; i < total_index; i++)
    {
        unsigned short c = 0;
        fb[i] = c;
    }
}

//rozsviti led pas, kdyz skonci hra
void winning_lights() // jedu nahoru a dolu po array
{
    struct timespec lights_delay = {.tv_sec = 0, .tv_nsec = 40 * 1000 * 1000};

    uint32_t val_line = 0xFF;
    char opakovani = 3;

    for (int j = 0; j < opakovani; j++)
    {
        for (int i = 0; i < 24; i++)
        {
            *(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
            val_line <<= 1;
            clock_nanosleep(CLOCK_MONOTONIC, 0, &lights_delay, NULL);
        }
        for (int i = 0; i < 24; i++)
        {
            *(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
            val_line >>= 1;
            clock_nanosleep(CLOCK_MONOTONIC, 0, &lights_delay, NULL);
        }
    }

}
